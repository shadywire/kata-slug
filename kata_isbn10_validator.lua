-- https://www.codewars.com/kata/51fc12de24a9d8cb0e000001
-- 5

local solution = {}
function solution.valid_ISBN10(isbn)
	local checksum = 0
	local i = 1
	for n in isbn:gmatch("([0-9X])") do
		if n == "X" then n = 10 else n = tonumber(n) end
		if n == 10 and i < 10 then return false end
		checksum = checksum + (i * n)
		i = i + 1
	end
	return i == 11 and checksum % 11 == 0
end

return solution

--print(solution.valid_ISBN10('1112223339'))
--print(solution.valid_ISBN10('048665088X'))
--print(solution.valid_ISBN10('0486650AAA'))
--print(solution.valid_ISBN10('1293'))
--print(solution.valid_ISBN10('X123456788'))

