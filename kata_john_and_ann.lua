-- https://www.codewars.com/kata/57591ef494aba64d14000526
-- 5

local solution = {}
local cache = {}
cache["john"] = {}
cache["john"][1] = 0
cache["ann"] = {}
cache["ann"][1] = 1

function solution.john(n)
	for i = 0,n,1 do solution.john_n(i) end
	for k,v in pairs(cache["john"]) do
		if k > n then break end
		rval[k] = v
	end
	return rval
end

function solution.john_n(n)
	local idx = n + 1
	if cache["john"][idx] ~= nil then return cache["john"][idx] end
	local val = n - solution.ann_n(solution.john_n(n-1))
	cache["john"][idx] = val
	return val
end

function solution.ann_n(n)
	local idx = n + 1
	if cache["ann"][idx] ~= nil then return cache["ann"][idx] end
	local val = n - solution.john_n(solution.ann_n(n-1))
	cache["ann"][idx] = val
	return val
end

function solution.ann(n)
	for i = 0,n-1,1 do solution.ann_n(i) end
	local rval = {}
	for k,v in pairs(cache["ann"]) do
		if k > n then break end
		rval[k] = v
	end
	return rval
end

function solution.sumJohn(n)
	local rval = 0
	for i = 0,n-1,1 do rval = rval + solution.john_n(i) end
	return rval
end

function solution.sumAnn(n)
	local rval = 0
	for i = 0,n-1,1 do rval = rval + solution.ann_n(i) end
	return rval
end

return solution

--print(unpack(solution.ann(6)))
--print(unpack(solution.john(14)))
--print(unpack(cache["john"]))
--print(unpack(cache["ann"]))
--print(solution.sumAnn(115))
--print(solution.sumJohn(78))
