-- https://www.codewars.com/kata/52449b062fb80683ec000024
-- 5

local solution = {}

function solution.generate_hashtag(s)
	local output = "#"
	for word in s:gmatch("(%S+)") do
		output = output .. string.lower(word):gsub("^%l", string.upper)
	end
	if #output == 1 or #output > 140 then return false else return output end
end

return solution

--print(solution.generate_hashtag("   hello World "))
