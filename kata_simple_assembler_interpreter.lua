-- https://www.codewars.com/kata/58e24788e24ddee28e000053
-- 5

kata = {}

function resolve(registers, val)
	if registers[val] == nil then return tonumber(val) else return tonumber(registers[val]) end
end

kata.simple_assembler = function(program)
	local R = {}
	local i = 1
	while i <= #program do
		local instruction = program[i]
		local args = {}
		for t in string.gmatch(instruction, "(%S+)") do table.insert(args, t) end
		local op = args[1]
		local inc = 1
		if op == "mov" then
			R[args[2]] = tonumber(resolve(R, args[3]))
		elseif op == "inc" then
			local arg1 = args[2]
			R[arg1] = R[arg1] + 1
		elseif op == "dec" then
			local arg1 = args[2]
			R[arg1] = R[arg1] - 1
		elseif op == "jnz" then
			if resolve(R, args[2]) ~= 0 then inc = resolve(R, args[3]) end
		end
		i = i + inc
	end
	return R
end

return kata

--local program = {'mov a 5', 'inc a', 'dec a', 'dec a', 'jnz a -1', 'inc a'} --> { a = 1 }
