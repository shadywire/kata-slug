-- https://www.codewars.com/kata/529adbf7533b761c560004e5/solutions/lua
-- 5

local solution = {}
local cache = {}
function mfib(n)
	if n == 2 or n == 1 then return 1 end
	if cache[n] ~= nil then
		return cache[n]
	else
		local value = mfib(n-1) + mfib(n-2)
		cache[n] = value
		return value
	end
end

function solution.fibonacci(n)
	return mfib(n)
end

return solution

--print(solution.fibonacci(50))
